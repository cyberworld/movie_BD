package cn.movie;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class Demo02_Movie_ImportToHBaseMR3 extends Configured implements Tool{
	@Override
	public int run(String[] args) throws Exception {
		// 输入
		if (args.length != 2) {
			System.out.println("usage :in out...");
			return -1;
		}
		
		Configuration config = HBaseConfiguration.create();
//		config.set("fs.defaultFS", "hdfs://hadoop31:8020");
		config.set("dfs.stream-buffer-size", "134217728"); //设置缓冲区大小，可以不定义
		FileSystem fs = FileSystem.get(config);
		Path path = new Path(args[1]);
		System.out.println(path);
		if (fs.exists(path)) {
			fs.delete(path, true);
		}
		//
		Job job = Job.getInstance(config,"Movie");
		job.setJarByClass(getClass());
		//
		job.setMapperClass(MyMapper.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(MovieInfo.class);
		job.setNumReduceTasks(0);//不需要Reducer
		//
		FileInputFormat.addInputPath(job, new Path(args[0]));// 设置输入的源
		FileOutputFormat.setOutputPath(job, path);
		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		int code = ToolRunner.run(new Demo02_Movie_ImportToHBaseMR3(), args);
		System.exit(code);
	}

	public static class MyMapper extends Mapper<LongWritable, Text, Text, MovieInfo> {
		Text text = new Text();
		private MovieInfo movie = null ;
		//
		private Connection con = null;
		private PreparedStatement ps = null;
		
		protected void setup(Mapper<LongWritable, Text, Text, MovieInfo>.Context context)
				throws IOException, InterruptedException {
			try {
				Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");//添加驱动
				String url="jdbc:phoenix:hadoop31:2181";
				con = DriverManager.getConnection(url);
				System.out.println("con:"+con);
				if (con!=null) {
					ps = con.prepareStatement("upsert into movie_info(IID,TYPE,NAME,YEAR,DIRECTOR,ACTOR) values(?,?,?,?,?,?)");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		@Override
		protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, MovieInfo>.Context context)
				throws IOException, InterruptedException {
			String uu = UUID.randomUUID().toString();
			uu = uu.replace("-", "");
			String[] strs = value.toString().split("\t");
						
			//向输出路径中写入数据：
			System.out.println("str:"+strs);
			text.set(uu);
			System.out.println("uu:"+uu);
			movie=new MovieInfo(uu,strs[0],strs[1],strs[2],strs[3],strs[4]);
			context.write(text, movie);
			
			//向hbase里写入数据：
			try {
				ps.setString(1, uu);
				ps.setString(2, strs[0]);
				ps.setString(3, strs[1]);
				ps.setString(4, strs[2]);
				ps.setString(5, strs[3]);
				ps.setString(6, strs[4]);
				int row = ps.executeUpdate();
				System.out.println("写入行数："+row);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		@Override //关闭连接！！！！！
		protected void cleanup(Mapper<LongWritable, Text, Text, MovieInfo>.Context context)
				throws IOException, InterruptedException {
			if (con!=null) {
				System.out.println("关闭连接："+con);
				try {
					con.commit();   //phoenix默认是关闭自动提交的（事务处于关闭状态），所以必须要commit()!!!!!!!
					ps.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
		
	public static class MovieInfo{
		private String id;
		private String name;
		private String type;
		private String year;
		private String director;
		private String actor;		
		
		@Override
		public String toString() {
			return id + "," + name + "," + type + "," + year + "," + director
					+ "," + actor;
		}
		public MovieInfo() {

		}
		public MovieInfo(String id, String name, String type, String year, String director, String actor) {
			super();
			this.id = id;
			this.name = name;
			this.type = type;
			this.year = year;
			this.director = director;
			this.actor = actor;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getYear() {
			return year;
		}
		public void setYear(String year) {
			this.year = year;
		}
		public String getDirector() {
			return director;
		}
		public void setDirector(String director) {
			this.director = director;
		}
		public String getActor() {
			return actor;
		}
		public void setActor(String actor) {
			this.actor = actor;
		}
		
		
	}	
}



