package cn.hbase.utils;


import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Table;

public class ConUtils {

	private static Connection con ;
	static {
		Configuration config = HBaseConfiguration.create();
		config.set("hbase.zookeeper.quorum", "hadoop31:2181");
		try {
			con = ConnectionFactory.createConnection(config);
		} catch (IOException e) {			
			e.printStackTrace();
		}
	}
	
	public static Connection getCon() {
		
		return con;		
	}
	
	public static Table getTable(String name) {
		System.out.println("con:"+con);
		Table table = null;
		try {
			table = con.getTable(TableName.valueOf(name));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return table;		
	}
	
	public static void close() {
		try {
			con.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
