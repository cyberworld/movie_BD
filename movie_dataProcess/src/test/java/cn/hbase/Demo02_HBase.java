//package cn.hbase;
//
//import org.apache.hadoop.conf.Configuration;
//import org.apache.hadoop.hbase.Cell;
//import org.apache.hadoop.hbase.CellScanner;
//import org.apache.hadoop.hbase.CellUtil;
//import org.apache.hadoop.hbase.CompareOperator;
//import org.apache.hadoop.hbase.HBaseConfiguration;
//import org.apache.hadoop.hbase.TableName;
//import org.apache.hadoop.hbase.client.Connection;
//import org.apache.hadoop.hbase.client.ConnectionFactory;
//import org.apache.hadoop.hbase.client.Result;
//import org.apache.hadoop.hbase.client.ResultScanner;
//import org.apache.hadoop.hbase.client.Scan;
//import org.apache.hadoop.hbase.filter.BinaryComparator;
//import org.apache.hadoop.hbase.filter.BinaryPrefixComparator;
//import org.apache.hadoop.hbase.filter.FilterList;
//import org.apache.hadoop.hbase.filter.FilterList.Operator;
//import org.apache.hadoop.hbase.filter.RegexStringComparator;
//import org.apache.hadoop.hbase.filter.RowFilter;
//import org.apache.hadoop.hbase.filter.SubstringComparator;
//import org.apache.hadoop.hbase.filter.ValueFilter;
//import org.apache.hadoop.hbase.util.Bytes;
//import org.junit.Test;
//
//public class Demo02_HBase {
//
//	@Test
//	public void test3() throws Exception {
//		Configuration config = HBaseConfiguration.create();
//		config.set("hbase.zookeeper.quorum", "hadoop81,hadoop82,hadoop83:2181");
//		Connection con = ConnectionFactory.createConnection(config);
//		Scan sc = new Scan();
//		TableName tn = TableName.valueOf("t02");
//		//根据rowkey进行查询，select * from xx where id=xxx
//		RowFilter row = new RowFilter(CompareOperator.EQUAL, new RegexStringComparator(".*0.*"));
//		sc.setFilter(row);
//		ResultScanner rs = con.getTable(tn).getScanner(sc);
//		for (Result r : rs) {
//			// 遍历cell
//			CellScanner cs = r.cellScanner();
//			while (cs.advance()) {
//				Cell cell = cs.current();
//				String rk = Bytes.toString(CellUtil.cloneRow(cell));
//				String cf = Bytes.toString(CellUtil.cloneFamily(cell));
//				String name = Bytes.toString(CellUtil.cloneQualifier(cell));
//				String value = Bytes.toString(CellUtil.cloneValue(cell));
//				System.out.println("rowkey" + rk + "\t" + cf + ":" + name + "=" + value);
//			}
//		}
//		rs.close();
//		con.close();
//	}
//
////多个条件
//	@Test
//	public void test2() throws Exception {
//		Configuration config = HBaseConfiguration.create();
//		config.set("hbase.zookeeper.quorum", "hadoop81,hadoop82,hadoop83:2181");
//		Connection con = ConnectionFactory.createConnection(config);
//		// 声明为or的关系
//		FilterList list = new FilterList(Operator.MUST_PASS_ONE);
//		ValueFilter filter1 = new ValueFilter(CompareOperator.EQUAL, new BinaryComparator("Jack".getBytes()));
//		list.addFilter(filter1);
//		ValueFilter filter2 = new ValueFilter(CompareOperator.EQUAL, new BinaryPrefixComparator("M".getBytes()));
//		list.addFilter(filter2);
//		TableName tn = TableName.valueOf("t02");
//
//		Scan sc = new Scan();
//		sc.setFilter(list);
//		ResultScanner rs = con.getTable(tn).getScanner(sc);
//		for (Result r : rs) {
//			// 遍历cell
//			CellScanner cs = r.cellScanner();
//			while (cs.advance()) {
//				Cell cell = cs.current();
//				String rk = Bytes.toString(CellUtil.cloneRow(cell));
//				String cf = Bytes.toString(CellUtil.cloneFamily(cell));
//				String name = Bytes.toString(CellUtil.cloneQualifier(cell));
//				String value = Bytes.toString(CellUtil.cloneValue(cell));
//				System.out.println("多个过虑器：" + rk + "\t" + cf + ":" + name + "=" + value);
//			}
//		}
//		rs.close();
//		con.close();
//	}
//
//	@Test
//	public void test1() throws Exception {
//		Configuration config = HBaseConfiguration.create();
//		config.set("hbase.zookeeper.quorum", "hadoop81,hadoop82,hadoop83:2181");
//		Connection con = ConnectionFactory.createConnection(config);
//		// 查询表
//		TableName tn = TableName.valueOf("t02");
//		// 创建查询的条件
//		Scan sc = new Scan();
//		ValueFilter filter1 = new ValueFilter(CompareOperator.EQUAL, new BinaryComparator("Jack".getBytes()));
//		sc.setFilter(filter1);
//		// 查询
//		ResultScanner rs = con.getTable(tn).getScanner(sc);
//		for (Result r : rs) {
//			// 遍历cell
//			CellScanner cs = r.cellScanner();
//			while (cs.advance()) {
//				Cell cell = cs.current();
//				String rk = Bytes.toString(CellUtil.cloneRow(cell));
//				String cf = Bytes.toString(CellUtil.cloneFamily(cell));
//				String name = Bytes.toString(CellUtil.cloneQualifier(cell));
//				String value = Bytes.toString(CellUtil.cloneValue(cell));
//				System.out.println(rk + "\t" + cf + ":" + name + "=" + value);
//			}
//		}
//		rs.close();
//		con.close();
//	}
//}
