package cn.hbase;

import java.util.ArrayList;
import java.util.List;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellScanner;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.TimestampsFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.Test;

import cn.hbase.utils.ConUtils;

public class Demo03_Filter {

	@Test
	public void test1() throws Exception {
		Table table = ConUtils.getTable("t01");
		Scan scan=new Scan();
		List<Long> timestamps = new ArrayList<>();
		timestamps.add(1534420055610L);
		TimestampsFilter tFilter = new TimestampsFilter(timestamps);
		scan.setFilter(tFilter);
		ResultScanner rs = table.getScanner(scan);
		show(rs);
		ConUtils.close();
	}

	public void show(ResultScanner rs) throws Exception {
		for (Result result : rs) {
			//遍历Cell
			CellScanner cs = result.cellScanner();
			while (cs.advance()) {
				Cell cell = cs.current();
				String rk = Bytes.toString(CellUtil.cloneRow(cell));
				String cf = Bytes.toString(CellUtil.cloneFamily(cell));
				String name = Bytes.toString(CellUtil.cloneQualifier(cell));
				String value = Bytes.toString(CellUtil.cloneValue(cell));
				System.out.println(rk + "\t" + cf + ":" + name + "=" + value);
			}
		}
		rs.close();
	}
	
	
/*	@Test
	public void testBatch() {
		Table table = ConUtils.getTable("t01");
		Scan scan =new Scan();
		List<Put> list = new ArrayList<>();
		for(int i=0;i<20;i++) {
			Put
		}
	}*/
	
	
	
}
