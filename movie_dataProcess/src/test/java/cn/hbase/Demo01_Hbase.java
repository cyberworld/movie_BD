package cn.hbase;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.junit.Test;

public class Demo01_Hbase {

	@Test
	public void test1() throws Exception {
		//获取配置信息
		Configuration configuration = HBaseConfiguration.create();
		//设置zookeeper
		configuration.set("hbase.zookeeper.quorum", "hadoop31:2181");
		//连接 
		Connection con = ConnectionFactory.createConnection(configuration);
		//查看连接是否成功
		System.out.println("connection："+con);
		//获取hbase的数据
		Admin admin = con.getAdmin();
		//获取某个数据库下所有的表
		TableName[] tb = admin.listTableNames();
		for (TableName tableName : tb) {
			System.out.println(tableName.getNameAsString());
		}
		admin.close();
		con.close();
	}
	
}
