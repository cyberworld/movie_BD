package cn.movie;

import java.net.URL;
import java.util.UUID;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

public class Demo02_Movie {
	@Test
	public void test1() throws Exception {
		String url ="https://movie.douban.com/subject/26767512/";
		Document dom =  Jsoup.parse(new URL(url),3000);
		String maker =  dom.select("#info span[class='attrs'] a[rel='v:directedBy']").text();
		System.out.println("导演:"+maker);
		
		String brief =  dom.select("#link-report span").text();
		System.out.println("简介："+brief);
		
		String mm =  dom.select("#comments-section span[class='pl']").text();
		System.out.println(mm);
		mm =  mm.replaceAll("[^0-9]", "");
		System.out.println("条数："+mm);
	}
}
