package cn.movie;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.movie.types.dao.ITypesDao;
import cn.movie.types.dao.impl.TypesDaoImpl;
import cn.movie.year.dao.IYearDao;

public class Demo01_ {
	@Test
	public void test1() {
		ApplicationContext ctx = 
				new ClassPathXmlApplicationContext("spring-jdbc.xml");
		ITypesDao daoImpl = ctx.getBean("typesDao", TypesDaoImpl.class);
		Object object = daoImpl.queryTypes();
		System.out.println("object:"+object);
	}
	
	@Test
	public void test2() {
		ApplicationContext context = new ClassPathXmlApplicationContext("spring-jdbc.xml");
		IYearDao yearDaoImpl = context.getBean("yearDao", IYearDao.class);
		Object object = yearDaoImpl.queryYear();
		System.out.println("object"+object);
	}
	
}
