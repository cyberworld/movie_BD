<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>YearType</title>
<script type="text/javascript" src="../resource/echarts.js"></script>
<script type="text/javascript" src="../resource/echarts.min.js"></script>
<script type="text/javascript" src="../resource/jquery-3.3.1.min.js"></script>
</head>
<body>
<p>影片数量</p>
<button id="btn">Loading</button>
<div id="show" style="width: 700px;height: 400px"></div>
</body>
<script type="text/javascript">
	$("#btn").click(function () {
		$.get("../year").done(function(json) {
			//1:使用Echarts初始化Dom元素
			var myChart = echarts.init(document.getElementById('show'));
			//2:获取后台json数据			
			var object=eval("("+json+")");
			var x = object['xAxis'];
			var v = object['values'];
			
			/* alert(json); */
			//3:声明数据
			var option = {
				title : {
					text : '影片年份统计'
				},
				tooltip : {},
				legend : {
					data : [ '影片数量' ]
				//数组
				},
				xAxis : {//x轴
					data : x
				},
				yAxis : {},
				series : [ {
					name : '数量',
					type : 'bar',
					data : v,
					label : {
						show : true,
						formatter : function(param) {
							console.log("param>" + param.data);
							if (param.data != "") {
								return  param.data;
							}
						}
					}
				} ]				
			};
			//4:
			 myChart.setOption(option); 
		})
	})


</script>
</html>