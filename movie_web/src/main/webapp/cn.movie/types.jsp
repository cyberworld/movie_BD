<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Movie_Types</title>
	<script type="text/javascript" src="../resource/echarts.js"></script>
	<script type="text/javascript" src="../resource/echarts.min.js"></script>
	<script type="text/javascript" src="../resource/jquery-3.3.1.min.js"></script>
</head>
<body>
	<p>主页</p>
	<button id="btn">Loading...</button>
	<div id="main" style="width: 1200px;height: 400px"></div>
</body>

<script type="text/javascript">

	$("#btn").click(function () {
		$.get("../types").done(function(json) {
			
			//1:使用echarts初始化Dom元素
			var myChart = echarts.init(document.getElementById("main"));
			
			var obj = eval("("+json+")");
			var x = obj['xAxis'];
			var v = obj['value']; 
			
			/* alert(json); 在页面打印输出*/
			
			//2:声明数据
			var option = {
				title : {
					text : '影片类型统计'
				},
				tooltip : {},
				legend : {
					data : [ '影片数量' ]
				//数组
				},
				xAxis : {//x轴
					data : x
				},
				yAxis : {},
				series : [ {
					name : '数量',
					type : 'bar',
					data : v,
					label : {
						show : true,
						formatter : function(param) {
							console.log("param>" + param.data);
							if (param.data != "") {
								return  param.data;
							}
						}
					}
				} ]				
			};
			//3:
			 myChart.setOption(option); 
		})
	});
	
</script>
</html>