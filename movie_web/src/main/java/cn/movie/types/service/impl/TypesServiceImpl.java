package cn.movie.types.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.movie.types.dao.ITypesDao;
import cn.movie.types.service.ITypesService;

@Service("typesService")
public class TypesServiceImpl implements ITypesService {

	//注入DAO,引用一下
	@Resource(name="typesDao")
	private ITypesDao dao;
	
	/* (non-Javadoc)
	 * @see cn.movie.types.service.impl.ITypesService#queryTypes()
	 */
	@Override
	public Map<String, Object> queryTypes(){
		return dao.queryTypes();
	}
}
