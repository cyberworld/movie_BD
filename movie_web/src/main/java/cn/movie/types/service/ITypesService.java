package cn.movie.types.service;

import java.util.Map;

public interface ITypesService {
	Map<String, Object> queryTypes();
}