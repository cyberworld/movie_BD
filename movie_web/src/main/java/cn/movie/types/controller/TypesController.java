package cn.movie.types.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import cn.movie.types.service.ITypesService;

@Controller
public class TypesController {

	//注入service
	@Resource(name="typesService")
	private ITypesService service;
	
	@RequestMapping("/types") //解决中文乱码,局部方法
	@ResponseBody  //默认格式：ISO-8859-1
	public String query() {
		Map<String, Object> map = service.queryTypes();
		
		//转成Json对象
		String json = JSONObject.toJSONString(map);		
		return json; //json.jsp		
	}
}
