package cn.movie.types.dao;

import java.util.Map;

public interface ITypesDao {
	// Map<key,list[]>
	Map<String, Object> queryTypes();
}