package cn.movie.utils;

import java.sql.Connection;

import javax.sql.DataSource;

import com.alibaba.druid.pool.DruidDataSource;

public class DBUtils {

	//使用连接池
	private static DataSource DATASOURCE;
	static {
		try {
			DruidDataSource ds = new DruidDataSource();
			ds.setDriverClassName("org.apache.phoenix.jdbc.PhoenixDriver");//添加驱动
			ds.setUrl("jdbc:phoenix:hadoop31:2181");
			ds.setMaxActive(3);//设置连接池最大活动个数：3
			ds.setMaxIdle(3000);//最长等待时间：3秒
			DATASOURCE = ds ;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static DataSource dataSource() {
		return DATASOURCE;
	}
}
