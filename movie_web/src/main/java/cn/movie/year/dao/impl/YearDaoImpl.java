package cn.movie.year.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.object.MappingSqlQuery;
import org.springframework.stereotype.Repository;

import cn.movie.year.dao.IYearDao;

@Repository("yearDao")
public class YearDaoImpl implements IYearDao{
	@Resource
	private DataSource dataSource;
	
	public Map<String, Object> queryYear(){
		MappingSqlQuery<Map<String, Long>> query = new MappingSqlQuery<Map<String,Long>>() {
			//每一行执行一次
			@Override
			protected Map<String, Long> mapRow(ResultSet rs, int rowNum) throws SQLException {
				Map<String, Long> map = new HashMap<>();
				map.put(rs.getString("year"), rs.getLong("cnt"));				
				return map;
			}
		};
		query.setDataSource(dataSource);
		String sql = "select year,count(year) as cnt from movie_Info where type='经典' group by year";
//		query.setQueryTimeout(3000);
		query.setSql(sql);
		List<Map<String, Long>> list = query.execute();
		System.err.println("list:"+list);
		
		//以下用于返回map
		List<String> xAxis = new ArrayList<>();
		List<Long> values = new ArrayList<>();
		for (Map<String, Long> map1 : list) {
			Map.Entry<String, Long> entry = map1.entrySet().iterator().next();
			xAxis.add(entry.getKey());
			values.add(entry.getValue());
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("xAxis", xAxis);
		map.put("values", values);
		System.out.println("map:"+map);
		return map;		
	}
 }
