package cn.movie.year.dao;

import java.util.Map;

public interface IYearDao {

	Map<String, Object> queryYear();

}