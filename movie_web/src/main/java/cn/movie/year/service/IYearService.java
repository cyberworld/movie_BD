package cn.movie.year.service;

import java.util.Map;

public interface IYearService {

	Map<String, Object> queryYear();

}