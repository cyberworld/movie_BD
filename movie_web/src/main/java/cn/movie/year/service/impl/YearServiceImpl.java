package cn.movie.year.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.movie.year.dao.IYearDao;
import cn.movie.year.service.IYearService;

@Service("yearService")
public class YearServiceImpl implements IYearService {

	@Resource(name="yearDao")
	private IYearDao dao;
	
	/* (non-Javadoc)
	 * @see cn.movie.year.service.IYearService#queryYear()
	 */
	@Override
	public Map<String, Object> queryYear() {
		return dao.queryYear();
	}
}
