package cn.movie.year.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.avro.data.Json;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import cn.movie.year.service.IYearService;

@Controller
public class YearController {
	@Resource(name="yearService")
	private IYearService service;
	
	@RequestMapping("/year")
	@ResponseBody
	public String query() {
		Map<String, Object> map = service.queryYear();
		//转成json格式
		String json = JSONObject.toJSONString(map);
		return json;
	}

}
