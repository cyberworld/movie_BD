package cn.movie;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Properties;
import org.junit.Test;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class Demo01_Movie {

	@Test
	public void test1() throws Exception {
		Properties pp = new Properties();
		pp.load(ClassLoader.getSystemResourceAsStream("movie.properties"));
		String url = pp.getProperty("url");
		System.out.println("url:"+url);
		
		String types = pp.getProperty("types");
//		System.out.println("types:"+types);
		String[] type = types.split(" ");
		for (String tp : type) {
			if (tp.trim().equals("")) {
				continue;
			}
			String typeEncode = URLEncoder.encode(tp, "UTF-8");
			String u1 = url.replace("${type}", typeEncode);
			int step = 20 ;
			for(int i=0;i<10000;i++) {
				String u2 = u1 + (i*step);
				down(u2);
			}
		}
	}
	
	public void down(String url0) throws Exception {
		URL url = new URL(url0);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setConnectTimeout(3000);
		con.setRequestProperty("Cookie", "ll=\"118220\"; bid=cyvxqCVfv7g; _vwo_uuid_v2=DD569FD4A5033DA1B8C78CF41C37F9BF8|b4a527d5950f9bf74657c5af0be70589; ap=1; ps=y; dbcl2=\"182888925:Yz4bQOUOVd8\"; __utmz=30149280.1534151335.3.3.utmcsr=douban.com|utmccn=(referral)|utmcmd=referral|utmcct=/; push_noty_num=0; push_doumail_num=0; ck=ydKV; __utmc=30149280; __utma=30149280.502131600.1533892243.1534553758.1534562255.5; ap_v=1,6.0");
		con.setDoInput(true);
		con.connect();
		int code = con.getResponseCode();
		System.out.println(code);
		if (code==200) {
			InputStream in = con.getInputStream();
			byte[] bs = new byte[1024*4];
			int len = 0;
			StringBuffer sBuffer = new StringBuffer();
			while((len=in.read(bs))!=-1) {
				sBuffer.append(new String(bs, 0, len));
			}
			System.out.println(sBuffer.toString());
			parseJson(sBuffer.toString());
			in.close();
		}
		con.disconnect();
	}

	public void parseJson(String json) throws Exception {
		PrintStream ps = new PrintStream(new FileOutputStream("D:/a/movie.txt", true));
		JSONObject obj = JSONObject.parseObject(json);
		JSONArray arr = obj.getJSONArray("subjects");
		for(int i=0;i<arr.size();i++) {
			obj = arr.getJSONObject(i);
			String id = obj.getString("id");
			String rate = obj.getString("rate");
			String cover_x = obj.getString("cover_x");
			String title = obj.getString("title");
			String url = obj.getString("url");
			ps.println(id+","+title+","+rate+","+cover_x+","+url);
		}
		ps.close();
	}
}
