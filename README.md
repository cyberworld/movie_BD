# 电影网站大数据分析

#### 项目介绍
项目一： hao123影视大数据分析
应用技术：MySQL+Spring+Spring MVC+Hadoop+Sqoop+HBase+Echarts+Ajax+JSON+JQuery+JSP
整个项目在STS中开发。应用Python从网站抓取数据；配置Spring，利用Spring MVC+DAO+Service开发模式，系统规范地开发项目；在JSP页面利用ECharts技术显示图表，并动态显示后台JSON数据；最终形成电影类型以及年份的统计图表。

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)